﻿#pragma once

// MFCActiveXControlTestPropPage.h: CMFCActiveXControlTestPropPage 属性页类的声明。


// CMFCActiveXControlTestPropPage : 请参阅 MFCActiveXControlTestPropPage.cpp 了解实现。

class CMFCActiveXControlTestPropPage : public COlePropertyPage
{
	DECLARE_DYNCREATE(CMFCActiveXControlTestPropPage)
	DECLARE_OLECREATE_EX(CMFCActiveXControlTestPropPage)

// 构造函数
public:
	CMFCActiveXControlTestPropPage();

// 对话框数据
	enum { IDD = IDD_PROPPAGE_MFCACTIVEXCONTROLTEST };

// 实现
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 消息映射
protected:
	DECLARE_MESSAGE_MAP()
};

