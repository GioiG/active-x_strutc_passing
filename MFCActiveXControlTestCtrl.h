﻿#pragma once

// MFCActiveXControlTestCtrl.h : CMFCActiveXControlTestCtrl ActiveX 控件类的声明。

typedef struct _tagMyUDT{
	int i;
	BSTR str;
}MyUDT;

// CMFCActiveXControlTestCtrl : 请参阅 MFCActiveXControlTestCtrl.cpp 了解实现。

class CMFCActiveXControlTestCtrl : public COleControl
{
	DECLARE_DYNCREATE(CMFCActiveXControlTestCtrl)

// 构造函数
public:
	CMFCActiveXControlTestCtrl();

// 重写
public:
	virtual void OnDraw(CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid);
	virtual void DoPropExchange(CPropExchange* pPX);
	virtual void OnResetState();
	virtual DWORD GetControlFlags();

// 实现
protected:
	~CMFCActiveXControlTestCtrl();

	DECLARE_OLECREATE_EX(CMFCActiveXControlTestCtrl)    // 类工厂和 guid
	DECLARE_OLETYPELIB(CMFCActiveXControlTestCtrl)      // GetTypeInfo
	DECLARE_PROPPAGEIDS(CMFCActiveXControlTestCtrl)     // 属性页 ID
	DECLARE_OLECTLTYPE(CMFCActiveXControlTestCtrl)		// 类型名称和杂项状态

// 消息映射
	DECLARE_MESSAGE_MAP()

// 调度映射
	DECLARE_DISPATCH_MAP()

// 事件映射
	DECLARE_EVENT_MAP()

// 调度和事件 ID
public:
	enum {
		dispidGetMyUdt = 4L,
		dispidGetVariantArray = 3L,
		dispidGetVariantInt = 2L,
		dispidGetByte = 1L
	};
protected:
	BYTE GetByte();
	VARIANT GetVariantInt();
	VARIANT GetVariantArray();
	VARIANT GetMyUdt();
};

