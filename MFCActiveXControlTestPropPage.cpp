﻿// MFCActiveXControlTestPropPage.cpp : CMFCActiveXControlTestPropPage 属性页类的实现。

#include "pch.h"
#include "framework.h"
#include "MFCActiveXControlTest.h"
#include "MFCActiveXControlTestPropPage.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

IMPLEMENT_DYNCREATE(CMFCActiveXControlTestPropPage, COlePropertyPage)

// 消息映射

BEGIN_MESSAGE_MAP(CMFCActiveXControlTestPropPage, COlePropertyPage)
END_MESSAGE_MAP()

// 初始化类工厂和 guid

IMPLEMENT_OLECREATE_EX(CMFCActiveXControlTestPropPage, "MFCACTIVEXCONT.MFCActiveXControlTestPropPage.1",
	0x5055e6c9,0x3392,0x4466,0xb5,0x85,0xb0,0x91,0x54,0xcb,0xd7,0x69)

// CMFCActiveXControlTestPropPage::CMFCActiveXControlTestPropPageFactory::UpdateRegistry -
// 添加或移除 CMFCActiveXControlTestPropPage 的系统注册表项

BOOL CMFCActiveXControlTestPropPage::CMFCActiveXControlTestPropPageFactory::UpdateRegistry(BOOL bRegister)
{
	if (bRegister)
		return AfxOleRegisterPropertyPageClass(AfxGetInstanceHandle(),
			m_clsid, IDS_MFCACTIVEXCONTROLTEST_PPG);
	else
		return AfxOleUnregisterClass(m_clsid, nullptr);
}

// CMFCActiveXControlTestPropPage::CMFCActiveXControlTestPropPage - 构造函数

CMFCActiveXControlTestPropPage::CMFCActiveXControlTestPropPage() :
	COlePropertyPage(IDD, IDS_MFCACTIVEXCONTROLTEST_PPG_CAPTION)
{
}

// CMFCActiveXControlTestPropPage::DoDataExchange - 在页和属性间移动数据

void CMFCActiveXControlTestPropPage::DoDataExchange(CDataExchange* pDX)
{
	DDP_PostProcessing(pDX);
}

// CMFCActiveXControlTestPropPage 消息处理程序
