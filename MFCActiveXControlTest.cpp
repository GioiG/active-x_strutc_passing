﻿// MFCActiveXControlTest.cpp: CMFCActiveXControlTestApp 和 DLL 注册的实现。

#include "pch.h"
#include "framework.h"
#include "MFCActiveXControlTest.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


CMFCActiveXControlTestApp theApp;

const GUID CDECL _tlid = {0x276ee5bf,0x423f,0x4cf4,{0xaa,0x93,0x51,0x71,0x0e,0xc3,0xe5,0x5b}};
const WORD _wVerMajor = 1;
const WORD _wVerMinor = 0;



// CMFCActiveXControlTestApp::InitInstance - DLL 初始化

BOOL CMFCActiveXControlTestApp::InitInstance()
{
	BOOL bInit = COleControlModule::InitInstance();

	if (bInit)
	{
		// TODO:  在此添加您自己的模块初始化代码。
	}

	return bInit;
}



// CMFCActiveXControlTestApp::ExitInstance - DLL 终止

int CMFCActiveXControlTestApp::ExitInstance()
{
	// TODO:  在此添加您自己的模块终止代码。

	return COleControlModule::ExitInstance();
}



// DllRegisterServer - 将项添加到系统注册表

STDAPI DllRegisterServer(void)
{
	AFX_MANAGE_STATE(_afxModuleAddrThis);

	if (!AfxOleRegisterTypeLib(AfxGetInstanceHandle(), _tlid))
		return ResultFromScode(SELFREG_E_TYPELIB);

	if (!COleObjectFactoryEx::UpdateRegistryAll(TRUE))
		return ResultFromScode(SELFREG_E_CLASS);

	return NOERROR;
}



// DllUnregisterServer - 将项从系统注册表中移除

STDAPI DllUnregisterServer(void)
{
	AFX_MANAGE_STATE(_afxModuleAddrThis);

	if (!AfxOleUnregisterTypeLib(_tlid, _wVerMajor, _wVerMinor))
		return ResultFromScode(SELFREG_E_TYPELIB);

	if (!COleObjectFactoryEx::UpdateRegistryAll(FALSE))
		return ResultFromScode(SELFREG_E_CLASS);

	return NOERROR;
}
